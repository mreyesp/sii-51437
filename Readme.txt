Para ejecutar la versión 1.4, es conveniente importar el script "tenis.sh" dentro de la carpeta src donde hayamos compilado los ejecutables. Este script ejecutará los procesos Logger, Cliente, Servidor y Bot de manera simultanea y en el orden en el que se deben ejecutar.

Instrucciones:

- El jugador 1 se moverá con las teclas W y S.
- El jugador 2 se moverá con las teclas O y L.
- El bot ejecutará las acciones del jugador 1.

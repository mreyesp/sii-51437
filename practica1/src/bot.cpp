#include <iostream>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>

#include "Esfera.h"
#include "DatosMemCompartida.h"
#include "MundoCliente.h"

int main(int argc, char const *argv[]) 
{
	char *mmapbot;
	DatosMemCompartida *puntdatos;
	
	int fdatos=open("/tmp/datos.txt",O_RDWR,0777);
	if(fdatos<0) {
		perror("ERROR Botmain");
	}
	mmapbot=(char*)mmap(NULL,sizeof(*(puntdatos)),PROT_WRITE | PROT_READ,MAP_SHARED,fdatos,0);
	close(fdatos);
	puntdatos=(DatosMemCompartida*)mmapbot;
	
	while(1)
	{
		
		float posiciony = (puntdatos->raqueta1.y2+puntdatos->raqueta1.y1)/2;

		if(puntdatos->control)
			exit(0);
		else if (posiciony<puntdatos->esfera.centro.y)	
			puntdatos->accion=1;
		else if (posiciony>puntdatos->esfera.centro.y)	
			puntdatos->accion=-1;
		else
			puntdatos->accion=0;
			
		usleep(25);
	}

	munmap(mmapbot,sizeof(*(puntdatos)));
	return 0;
	
}
